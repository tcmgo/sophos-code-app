import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from "../pages/list/list";

import "reflect-metadata";
import {createConnection} from "typeorm";
import {Leitura} from "../modelo/leitura";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen) {

    this.initializeApp();

    this.pages = [
      { title: 'Leitura de QR Code', component: HomePage },
      { title: 'Listagem', component: ListPage }
    ];

  }

  initializeApp() {

    this.platform.ready().then(async () => {

      let modelo = [
        Leitura
      ]

      if(this.platform.is('cordova')) {

        await createConnection({
          type: 'cordova',
          database: 'test',
          location: 'default',
          logging: ['error', 'query', 'schema'],
          synchronize: true,
          entities: modelo
        });

      } else {

        await createConnection({
          type: 'sqljs',
          autoSave: true,
          location: 'browser',
          logging: ['error', 'query', 'schema'],
          synchronize: true,
          entities: modelo
        });

      }

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {

    this.nav.setRoot(page.component);
  }
}

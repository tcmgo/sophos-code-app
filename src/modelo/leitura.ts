import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";

@Entity('tb_leituras')
export class Leitura {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dataInclusao: Date = new Date();

  @Column()
  sincronizado: boolean = false;

  @Column()
  qrCode: string;

  @Column()
  eventoId: number;

  @Column()
  participanteId: number;

  @Column()
  eventoNome: string;

  @Column()
  participanteNome: string;
}

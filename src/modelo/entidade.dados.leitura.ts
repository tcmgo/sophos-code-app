export class EntidadeDadosLeitura {

  eventoId: number;

  participanteId: number;

  eventoNome: string;

  participanteNome: string;
}

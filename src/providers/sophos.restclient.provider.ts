import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Modulo} from "../modelo/modulo";
import {PresencaQRCode} from "../modelo/presenca.qr.code";

@Injectable()
export class SophosRestclientProvider {

  urlRest: string = "http://localhost:8080/<contexto>/rest/";

  constructor(private httpClient: HttpClient) {}

  public obterModulosEvento(eventoId: number): Promise<Modulo[]> {

    const url = this.urlRest + "obter-modulos-evento/" + eventoId.toString();

    return this.httpClient.get<Modulo[]>(url).toPromise();
  }

  public enviarFrequenciasSophos(presencasQRCode: PresencaQRCode[]): Promise<any> {

    const url = this.urlRest + "carregar-presencas/";

    return this.httpClient.post<any>(url, presencasQRCode).toPromise();
  }

}

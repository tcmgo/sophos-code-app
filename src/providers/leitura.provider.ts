import {Injectable} from "@angular/core";
import {getRepository, Repository} from "typeorm";
import {Leitura} from "../modelo/leitura";
import {EntidadeDadosLeitura} from "../modelo/entidade.dados.leitura";

@Injectable()
export class LeituraProvider {

  protected repository: Repository<Leitura>;

  constructor(){}

  prepararLeiturasParaEnvio(leituras: Leitura[]): Leitura[] {

    if(leituras) {

      let leiturasPreparadas : Leitura[] = new Array();

      leituras.forEach(value => {

        let participanteRepetido = leiturasPreparadas.find(value1 => value1.participanteId == value.participanteId);

        if(!participanteRepetido && (value.sincronizado == false)) {

          leiturasPreparadas.push(value);
        }
      });

      return leiturasPreparadas;
    }

    return null;
  }

  incluirLeitura(leituraQRCode: string): Promise<any> {

    this.checkRepository();

    let leitura : Leitura = new Leitura();

    leitura.qrCode = leituraQRCode;

    let entidadeDadosLeitura : EntidadeDadosLeitura = JSON.parse(leituraQRCode);

    leitura.participanteNome = entidadeDadosLeitura.participanteNome;

    leitura.participanteId = entidadeDadosLeitura.participanteId;

    leitura.eventoId = entidadeDadosLeitura.eventoId;

    leitura.eventoNome = entidadeDadosLeitura.eventoNome;

    return this.repository.save(leitura);
  }

  atualizarSincronizacaoLeitura(participanteId: number) {

    this.checkRepository();

    return this.repository.createQueryBuilder().update(Leitura).set({sincronizado: true}).where("participanteId = :participanteId", { participanteId: participanteId}).execute();
  }

  findAll(): Promise<Leitura[]>{

    this.checkRepository();

    return this.repository.find({
      order: {
        dataInclusao: "DESC"
      }
    })
  }

  private checkRepository(){

    if(!this.repository){

      this.repository = getRepository(Leitura);
    }
  }
}

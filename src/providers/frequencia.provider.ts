import {Injectable} from "@angular/core";
import {PresencaQRCode} from "../modelo/presenca.qr.code";
import {Leitura} from "../modelo/leitura";

@Injectable()
export class FrequenciaProvider {

  constructor(){}

  converterLeiturasEmFrequencias (leituras: Leitura[]): PresencaQRCode[] {

    let frequenciaAtual : PresencaQRCode;

    let frequencias : PresencaQRCode[] = new Array();

    if(leituras) {

      leituras.forEach(value => {

        frequenciaAtual = new PresencaQRCode();

        frequenciaAtual.eventoId = value.eventoId;

        frequenciaAtual.participanteId = value.participanteId;

        frequenciaAtual.moduloId = null;

        frequenciaAtual.numeroEncontro = null;

        frequencias.push(frequenciaAtual);
      });

      return frequencias;
    }
    return null;
  }
}

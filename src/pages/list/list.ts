import {ChangeDetectorRef, Component} from '@angular/core';
import { NavController, NavParams, ModalController} from 'ionic-angular';
import {LeituraProvider} from "../../providers/leitura.provider";
import {Leitura} from "../../modelo/leitura";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  leituras: Leitura[];

  leiturasEventoGrupos = [];

  constructor(private navCtrl: NavController,
              private changeDetect: ChangeDetectorRef,
              private navParams: NavParams,
              private leituraProvider: LeituraProvider,
              private modalController: ModalController) {

  }

  ionViewDidEnter(){

    this.carregarLeiturasAgrupadas();
  }

  public carregarLeiturasAgrupadas() {

      this.leituraProvider.findAll().then(res => {

        console.log(res);

        this.leituras = res;

        this.agruparLeiturasPorEvento();

        this.changeDetect.detectChanges();
      })
  }

  agruparLeiturasPorEvento() {

    this.leiturasEventoGrupos = [];

    let eventoIdAtual = -5;

    let leiturasAtuais = [];

    for (let value of this.leituras) {

      if(value.eventoId != eventoIdAtual) {

        eventoIdAtual = value.eventoId;

        let novoGrupo = {

          grupoEventoId: eventoIdAtual,

          grupoEventoNome: value.eventoNome,

          leiturasDesteGrupo: []
        };

        leiturasAtuais = novoGrupo.leiturasDesteGrupo;

        this.leiturasEventoGrupos.push(novoGrupo);
      }
      leiturasAtuais.push(value);
    }
  }

  acaoEnviarLeiturasEventoSophos(leituras: Leitura[], eventoNome: string, eventoId: number) {

    let leiturasPreparadasEnvio = this.leituraProvider.prepararLeiturasParaEnvio(leituras);

    const modalEnvio = this.modalController.create('ModalEnviarFrequenciasSophosPage', { ArrayLeituras: leiturasPreparadasEnvio , EventoNome: eventoNome, EventoId: eventoId});

    modalEnvio.onDidDismiss(() => this.carregarLeiturasAgrupadas());

    modalEnvio.present();
  }
}

import { Component } from '@angular/core';
import {Alert, AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Leitura} from "../../modelo/leitura";
import {SophosRestclientProvider} from "../../providers/sophos.restclient.provider";
import {FrequenciaProvider} from "../../providers/frequencia.provider";
import {PresencaQRCode} from "../../modelo/presenca.qr.code";
import {Modulo} from "../../modelo/modulo";
import {LeituraProvider} from "../../providers/leitura.provider";

/**
 * Generated class for the ModalEnviarFrequenciasSophosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-enviar-frequencias-sophos',
  templateUrl: 'modal-enviar-frequencias-sophos.html',
})
export class ModalEnviarFrequenciasSophosPage {

  leituras: Leitura[];

  frequencias: PresencaQRCode[];

  eventoNome: string;

  eventoId: number;

  modulos: Modulo[];

  moduloId: number;

  numeroEncontro: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewController: ViewController,
              private sophosClientrestProvider: SophosRestclientProvider,
              private frequenciaProvider: FrequenciaProvider,
              private alertController: AlertController,
              private leituraProvider: LeituraProvider) {
  }

  async ionViewWillEnter() {

    this.leituras = this.navParams.get('ArrayLeituras');

    this.eventoNome = this.navParams.get('EventoNome');

    this.eventoId = this.navParams.get('EventoId');

    if(this.leituras.length > 0) {

      this.frequencias = this.frequenciaProvider.converterLeiturasEmFrequencias(this.leituras);

      this.modulos = await this.sophosClientrestProvider.obterModulosEvento(this.eventoId);
    }
  }

  public nomeModal(): string {

    return ("Enviar presenças " + this.eventoNome);
  }

  private async atualizarSincronizacaoLeiturasEnviadas() {

    for (let value of this.leituras) {

      await this.leituraProvider.atualizarSincronizacaoLeitura(value.participanteId);
    }
  }

  async enviarFrequenciasSophos() {

    let alertaTemporario = this.alertController.create({

      title: "Fazendo envio...aguarde!",

      message: "Enviando",

    })

    if(this.frequencias.length > 0 && this.moduloId >= 0 && this.numeroEncontro > 0) {

      this.frequencias.forEach(value => {

        value.moduloId = this.moduloId;

        value.numeroEncontro = this.numeroEncontro;
      });

      try {

        let alerta: Alert;

        alertaTemporario.present();

        await this.sophosClientrestProvider.enviarFrequenciasSophos(this.frequencias);

        this.atualizarSincronizacaoLeiturasEnviadas();

        this.closeModal();

        alertaTemporario.dismiss();

        alerta = this.criarAlerta("Envio realizado com sucesso!", "Envio SOPHOS");

        alerta.present();

      } catch (e) {

        this.closeModal();

        alertaTemporario.dismiss();

        this.criarAlerta("Erro no envio. Favor, checar encontro e módulo!", "Falha SOPHOS").present();
      }
    }
    else {

      this.criarAlerta("Informe um módulo e um encontro válido!", "Campos incorretos!").present();
    }
  }

  criarAlerta(aviso: string, titulo: string): Alert {

    let alerta = this.alertController.create({

      title: titulo,

      message: aviso,

      buttons: ['OK']
    })

    return alerta;
  }

  closeModal() {

    this.viewController.dismiss();
  }
}

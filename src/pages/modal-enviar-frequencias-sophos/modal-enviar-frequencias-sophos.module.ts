import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEnviarFrequenciasSophosPage } from './modal-enviar-frequencias-sophos';

@NgModule({
  declarations: [
    ModalEnviarFrequenciasSophosPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEnviarFrequenciasSophosPage),
  ],
})
export class ModalEnviarFrequenciasSophosPageModule {}

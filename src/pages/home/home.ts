import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {LeituraProvider} from "../../providers/leitura.provider";
import {Leitura} from "../../modelo/leitura";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lista: any = []

  constructor(private navCtrl: NavController,
              private barcodeScanner: BarcodeScanner,
              private platform: Platform,
              private leituraProvider: LeituraProvider) {

  }

  escanearQrCode() {


    this.leituraProvider.incluirLeitura("{\"eventoId\":1111,\"participanteId\":1111,\"eventoNome\":\"(TESTE) - 6º CONGRESSO INTERNACIONAL DE DIREITO FINANCEIRO\",\"participanteNome\":\"José Alves\"}");

    /*if (this.platform.is('cordova')) {

      this.barcodeScanner.scan().then(barcodeData => {

        this.leituraProvider.incluirLeitura(barcodeData.text).then(res => {

            this.lista.push(barcodeData.text)

          }
        )

      }).catch(err => {

        console.log('Error', err);
      })
    }*/
  }
}
